# Notes

## Core Mongo

### CRUD
- Create
    - `.save()`
- Reading
    - `.find(criteria)`
        - find all return ARRAY
    - `.findOne(criteria)`
        - find first return single record
- Update
    - ``
        -
- Destroy (remove)
    - `.remove()`
        - remove all
    - `.findOneAndRemove`
        - remove first
    -`.findByIdAndRemove`
    - `$set`
        - https://docs.mongodb.com/manual/reference/operator/update/
