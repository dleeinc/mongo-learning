const assert = require('assert');
const User = require('../src/user');

describe('Reading Users records', () => {
    let destin;

    beforeEach((done) => {
        destin = new User({ name: 'Destin'});
        destin.save()
            .then(() => done());
    });

    it('it reads a user', (done) => {
        User.find({ name: 'Destin' })
            .then((users) => {
                assert(users[0]._id.toString() === destin._id.toString());
                done();
            });
    });

    it('find a user with ID', (done) => {
        User.findOne({ _id: destin._id })
            .then((user) => {
                assert(user.name === 'Destin');
                done();
            });
    });
});
