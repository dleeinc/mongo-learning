const assert = require('assert');
const User = require('../src/user');

describe('subdocs test', () => {
    it('it can create a sub document', (done) => {
        const destin = new User({
            name: 'Destin',
            posts: [{title: 'This is a post title'},{title:'this is another title'}]
        });
        destin.save()
            .then(() => User.findOne({name: 'Destin'}))
            .then((user) => {
                assert(user.posts[0].title === 'This is a post title');
                done();
            });
    });

    it('addes to the subdoc', (done) => {
        const destin = new User({
            name: 'Destin',
            posts: []
        });

        destin.save()
            .then(() => User.findOne({name: 'Destin'}))
            .then((user) => {
                user.posts.push({title: 'New post'});
                return user.save(); // needs to retuen so that we get a new Promise
            })
            .then(() => User.findOne({name: 'Destin'}))
            .then((user) => {
                assert(user.posts[0].title === 'New post');
                done();
            })
    })

    it('delets a subdoc', (done) => {
        const destin = new User({
            name: 'Destin',
            posts: [{title: 'new title'}] // Sets the post
        });

        destin.save()
            .then(() => User.findOne({name: 'Destin'}))
            .then((user) => {
                const post = user.posts[0];
                post.remove(); // delets the post
                return user.save();
            })
            .then(() => User.findOne({name: 'Destin'}))
            .then((user) => {
                assert(user.posts.length === 0);
                done();
            })
    });
});
