const assert = require('assert');
const User = require('../src/user');

describe('Creating records', () => {
    it('it saves a user', (done) => {
        const destin = new User({name: 'Destin'})

        destin.save()
            .then(() => {
                assert(!destin.isNew);
                done();
            });
    });
});
