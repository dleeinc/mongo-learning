const assert = require('assert');
const User = require('../src/user');

describe('Update Users records', () => {
    let destin;

    beforeEach((done) => {
        destin = new User({ name: 'Destin', postCount: 0});
        destin.save()
            .then(() => done());
    });

    function assertname(operation, done) {
        operation
            .then(() => User.find())
            .then((users) => {
                assert(users.length === 1);
                assert(users[0].name === 'Stacy');
                done();
            })
    }

    it('model instance set and save', (done) => {
        destin.set('name', 'Stacy');
        assertname(destin.save(), done);

    });

    it('model instance update', (done) => {
        assertname(destin.update({name: 'Stacy'}), done);
    });

    // Class based updates

    it('modle class can update', (done) => {
        assertname(
            User.update({name: 'Destin'}, {name: 'Stacy'}),
            done
        );
    });

    it('modle class can update one record', (done) => {
        assertname(
            User.findOneAndUpdate({name: 'Destin'}, {name: 'Stacy'}),
            done
        );
    });

    it('modle class can find a record by Id and update', (done) => {
        assertname(
            User.findByIdAndUpdate(destin._id, {name: 'Stacy'}),
            done
        );
    });

    // mongo update modifiers
    // https://docs.mongodb.com/manual/reference/operator/update/

    it('a user can have post count change', (done) => {
        User.update({name: 'Destin'}, {$inc: {postCount: 1}})
            .then(() => User.findOne({name: 'Destin'}))
            .then((user) => {
                assert(user.postCount === 1);
                done();
            });
    });
});
