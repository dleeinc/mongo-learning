const assert = require('assert');
const User = require('../src/user');

describe('Destory records', () => {
    let destin;

    beforeEach((done) => {
        destin = new User({name: 'Destin'});
        destin.save()
            .then(() => done());
    });

    it('model instance remove', (done) => {
        destin.remove()
            .then(() => User.findOne({name: 'Destin'}))
            .then((user) => {
                assert(user === null);
                done();
            });
    });

    it('class method remove', (done) => {
        User.remove({name:'Destin'})
            .then(() => User.findOne({name: 'Destin'}))
            .then((user) => {
                assert(user === null);
                done();
            });
    });

    it('class method findAndRemove', (done) => {
        User.findOneAndRemove({name: 'Destin'})
            .then(() => User.findOne({name: 'Destin'}))
            .then((user) => {
                assert(user === null);
                done();
            });
    });

    it('class method findByIdAndRemove', (done) => {
        User.findByIdAndRemove(destin._id)
            .then(() => User.findOne({name: 'Destin'}))
            .then((user) => {
                assert(user === null);
                done();
            });
    });
});
