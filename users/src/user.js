const mongoose = require('mongoose');
const PostSchema = require('./post');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    name: {
        type: String,
        validate: {
            validator: (name) => name.length > 2,
            message: 'Name must be longer than 2 char.'
        },
        required: [true, 'You need to provide a name'],

    },
    postCount: Number,
    posts: [PostSchema]
});

const User = mongoose.model('user', UserSchema);

module.exports = User;
